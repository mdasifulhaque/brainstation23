USE [master]
GO
/****** Object:  Database [BRAINSTATION]    Script Date: 11/27/2020 9:30:22 PM ******/
CREATE DATABASE [BRAINSTATION]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BRAINSTATION', FILENAME = N'E:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\BRAINSTATION.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'BRAINSTATION_log', FILENAME = N'E:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\BRAINSTATION_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [BRAINSTATION] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BRAINSTATION].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BRAINSTATION] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BRAINSTATION] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BRAINSTATION] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BRAINSTATION] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BRAINSTATION] SET ARITHABORT OFF 
GO
ALTER DATABASE [BRAINSTATION] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BRAINSTATION] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BRAINSTATION] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BRAINSTATION] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BRAINSTATION] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BRAINSTATION] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BRAINSTATION] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BRAINSTATION] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BRAINSTATION] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BRAINSTATION] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BRAINSTATION] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BRAINSTATION] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BRAINSTATION] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BRAINSTATION] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BRAINSTATION] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BRAINSTATION] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BRAINSTATION] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BRAINSTATION] SET RECOVERY FULL 
GO
ALTER DATABASE [BRAINSTATION] SET  MULTI_USER 
GO
ALTER DATABASE [BRAINSTATION] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BRAINSTATION] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BRAINSTATION] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BRAINSTATION] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [BRAINSTATION] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [BRAINSTATION] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'BRAINSTATION', N'ON'
GO
ALTER DATABASE [BRAINSTATION] SET QUERY_STORE = OFF
GO
USE [BRAINSTATION]
GO
/****** Object:  Table [dbo].[post]    Script Date: 11/27/2020 9:30:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[post](
	[post_id] [int] IDENTITY(1,1) NOT NULL,
	[post_name] [ntext] NOT NULL,
	[post_text] [ntext] NULL,
	[post_author] [varchar](50) NOT NULL,
	[post_date] [datetime] NOT NULL,
 CONSTRAINT [PK_post] PRIMARY KEY CLUSTERED 
(
	[post_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[reaction]    Script Date: 11/27/2020 9:30:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[reaction](
	[reaction_like] [int] NOT NULL,
	[reaction_dislike] [int] NOT NULL,
	[comment_id] [int] NOT NULL,
	[reaction_id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_reaction] PRIMARY KEY CLUSTERED 
(
	[reaction_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[comment]    Script Date: 11/27/2020 9:30:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[comment](
	[comment_id] [int] IDENTITY(1,1) NOT NULL,
	[comment_text] [text] NOT NULL,
	[comment_author] [varchar](50) NOT NULL,
	[post_id] [int] NOT NULL,
	[comment_date] [date] NULL,
 CONSTRAINT [PK_comment] PRIMARY KEY CLUSTERED 
(
	[comment_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[ReportData]    Script Date: 11/27/2020 9:30:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ReportData]
AS
SELECT        dbo.post.post_id, dbo.post.post_name, dbo.post.post_text, dbo.post.post_author, dbo.post.post_date, dbo.comment.comment_id, dbo.comment.comment_text, dbo.comment.comment_author, dbo.comment.comment_date, 
                         dbo.reaction.reaction_dislike, dbo.reaction.reaction_like, dbo.reaction.reaction_id
FROM            dbo.comment INNER JOIN
                         dbo.post ON dbo.comment.post_id = dbo.post.post_id FULL OUTER JOIN
                         dbo.reaction ON dbo.comment.comment_id = dbo.reaction.comment_id
GO
ALTER TABLE [dbo].[post]  WITH CHECK ADD  CONSTRAINT [FK_post_post] FOREIGN KEY([post_id])
REFERENCES [dbo].[post] ([post_id])
GO
ALTER TABLE [dbo].[post] CHECK CONSTRAINT [FK_post_post]
GO
ALTER TABLE [dbo].[reaction]  WITH CHECK ADD  CONSTRAINT [FK_reaction_comment] FOREIGN KEY([comment_id])
REFERENCES [dbo].[comment] ([comment_id])
GO
ALTER TABLE [dbo].[reaction] CHECK CONSTRAINT [FK_reaction_comment]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "post"
            Begin Extent = 
               Top = 6
               Left = 257
               Bottom = 136
               Right = 427
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "comment"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 219
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "reaction"
            Begin Extent = 
               Top = 6
               Left = 465
               Bottom = 136
               Right = 635
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 13
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ReportData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ReportData'
GO
USE [master]
GO
ALTER DATABASE [BRAINSTATION] SET  READ_WRITE 
GO
