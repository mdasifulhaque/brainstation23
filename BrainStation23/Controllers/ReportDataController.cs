﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BrainStation23.Models;

namespace BrainStation23.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportDataController : ControllerBase
    {
        private readonly ReportDataDbContext _context;

        public ReportDataController(ReportDataDbContext context)
        {
            _context = context;
        }

        // GET: api/ReportData
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReportData>>> GetReportData()
        {
            return await _context.ReportData.ToListAsync();
        }

        // GET: api/ReportData/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ReportData>> GetReportData(int id)
        {
            var reportData = await _context.ReportData.FindAsync(id);

            if (reportData == null)
            {
                return NotFound();
            }

            return reportData;
        }

        // PUT: api/ReportData/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutReportData(int id, ReportData reportData)
        {
            if (id != reportData.post_id)
            {
                return BadRequest();
            }

            _context.Entry(reportData).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReportDataExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ReportData
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ReportData>> PostReportData(ReportData reportData)
        {
            _context.ReportData.Add(reportData);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetReportData", new { id = reportData.post_id }, reportData);
        }

        // DELETE: api/ReportData/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ReportData>> DeleteReportData(int id)
        {
            var reportData = await _context.ReportData.FindAsync(id);
            if (reportData == null)
            {
                return NotFound();
            }

            _context.ReportData.Remove(reportData);
            await _context.SaveChangesAsync();

            return reportData;
        }

        private bool ReportDataExists(int id)
        {
            return _context.ReportData.Any(e => e.post_id == id);
        }
    }
}
