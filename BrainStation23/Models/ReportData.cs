﻿
using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.VisualBasic;


namespace BrainStation23.Models
{
	public class ReportData
	{
		[Key] public int post_id { get; set; }

		public string post_name { get; set; }
		public string post_author { get; set; }
		public DateTime post_date { get; set; }
		public int comment_id { get; set; }
		public string comment_author { get; set; }
		public DateTime comment_date { get; set; }
		public int reaction_id { get; set; }
		public int reaction_like { get; set; }
		public int reaction_dislike { get; set; }


	}
}
