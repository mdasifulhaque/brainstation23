﻿using System;

using System.ComponentModel.DataAnnotations;


namespace BrainStation23.Models
{
	public class Post
	{
		[Key]
		public int post_id { get; set; }
		public string post_name { get; set; }
		public string post_author{ get; set; }
		public DateTime post_date { get; set; }
	}
}
