﻿using Microsoft.EntityFrameworkCore;


namespace BrainStation23.Models
{
	public class ReportDataDbContext:DbContext
	{
		public ReportDataDbContext(DbContextOptions<ReportDataDbContext> options) : base(options)
		{

		}
		// public ReportDataDbContext()
		// {
		//
		// }
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<ReportData>().ToTable("ReportData");
		}

		public DbSet<ReportData> ReportData { get; set; }
	}
}
