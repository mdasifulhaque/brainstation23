﻿using Microsoft.EntityFrameworkCore;


namespace BrainStation23.Models
{
	public class PostDbContext:DbContext
	{
		public PostDbContext(DbContextOptions<PostDbContext> options) : base(options)
		{

		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Post>().ToTable("post");
		}

		public DbSet<Post> Post { get; set; }
	}
}
